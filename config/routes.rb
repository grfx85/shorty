# == Route Map
#
#                    Prefix Verb URI Pattern                                                                              Controller#Action
#                      root GET  /                                                                                        short_urls#index
#         short_urls_create POST /short_urls/create(.:format)                                                             short_urls#create
#                           GET  /:short_url(.:format)                                                                    short_urls#show
#                 shortened GET  /shortened/:short_url(.:format)                                                          short_urls#shortened
#        rails_service_blob GET  /rails/active_storage/blobs/:signed_id/*filename(.:format)                               active_storage/blobs#show
# rails_blob_representation GET  /rails/active_storage/representations/:signed_blob_id/:variation_key/*filename(.:format) active_storage/representations#show
#        rails_disk_service GET  /rails/active_storage/disk/:encoded_key/*filename(.:format)                              active_storage/disk#show
# update_rails_disk_service PUT  /rails/active_storage/disk/:encoded_token(.:format)                                      active_storage/disk#update
#      rails_direct_uploads POST /rails/active_storage/direct_uploads(.:format)                                           active_storage/direct_uploads#create

Rails.application.routes.draw do
  root to: 'short_urls#index'
  post 'short_urls/create'
  get '/:short_url', to: 'short_urls#show'
  get 'shortened/:short_url', to: 'short_urls#shortened', as: :shortened
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end

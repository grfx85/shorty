require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Shorty
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.2
    config.encoding = 'utf-8'
    config.i18n.available_locales = [:en]
    config.i18n.default_locale = :en
    config.time_zone = 'Warsaw'
    config.active_record.default_timezone = :local

    config.active_record.schema_format = :sql

    config.generators.test_framework false
    config.generators.stylesheets = false
    config.generators.javascripts = false
    config.generators do |g|
      g.template_engine :haml
      g.test_framework :rspec,
                       fixtures:         true,
                       view_specs:       false,
                       helper_specs:     false,
                       routing_specs:    false,
                       controller_specs: true,
                       request_specs:    true

      g.factory_bot dir: 'spec/factories'
    end
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.
  end
end

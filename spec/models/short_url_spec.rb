# == Schema Information
#
# Table name: short_urls
#
#  id           :bigint(8)        not null, primary key
#  original_url :text
#  short_url    :string
#  sanitize_url :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

require 'rails_helper'

RSpec.describe ShortUrl, type: :model do
  subject { build(:short_url) }

  describe 'validations' do
    it { should validate_presence_of(:original_url).on(:create) }
    it { should allow_value('http://test.test.pl').for(:original_url) }
    it { should_not allow_value('//http://test.test.pl////').for(:original_url) }
  end

  describe 'before callbacks' do
    it 'fills short_url value' do
      expect { subject.save }.to change { subject.short_url }
    end
  end

  describe 'methods' do
    before { create(:short_url) }

    context '#create_short_url' do
      it 'creates a short url' do
        expect { subject.create_short_url }.to change { subject.short_url }
      end

      it 'dose not receive #create_short_url' do
        expect(subject).to receive(:create_short_url).once
        subject.create_short_url
      end
    end

    context '#check_duplicates' do
      context 'duplicates present' do
        it 'checks for duplicates' do
          expect(ShortUrl.last.check_duplicates).to eq ShortUrl.first
        end
      end

      context 'duplicates not present' do
        it 'checks for duplicates' do
          expect(subject.check_duplicates).to eq nil
        end
      end
    end

    context '#fresh_url?' do
      context 'duplicates present' do
        it 'checks if fresh_url?' do
          expect(ShortUrl.last.fresh_url?).to eq false
        end
      end

      context 'duplicates not present' do
        it 'checks if fresh_url?' do
          expect(subject.fresh_url?).to eq true
        end
      end
    end

    context '#sanitize_original_url' do
      subject { build(:short_url, original_url: 'www.onet.pl') }
      it 'checks if fresh_url?' do
        expect(subject.sanitize_original_url).to eq 'http://onet.pl'
      end
    end
  end
end

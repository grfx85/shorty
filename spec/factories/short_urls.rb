# == Schema Information
#
# Table name: short_urls
#
#  id           :bigint(8)        not null, primary key
#  original_url :text
#  short_url    :string
#  sanitize_url :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

FactoryBot.define do
  factory :short_url do
    original_url { 'http://test.test.pl' }
  end
end

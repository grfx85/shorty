require 'rails_helper'

feature 'User creates a shortened link' do
  before { create(:short_url, original_url: 'www.test.com') }
  scenario 'entering root and creating a short link' do
    visit root_path
    expect(page).to have_content('Size matter')
    expect(page).to have_content('Enter URL')
    fill_in('original_url', with: 'aaaaa')
    click_button 'Shorten!'
    expect(page).to have_content('Original url Invalid url format!')
    visit root_path
    fill_in('original_url', with: 'www.test.com')
    click_button 'Shorten!'
    expect(page).to have_content('original url: (http://test.com)')
    expect(page).to have_content("short url: (#{Capybara.current_session.server.host}:#{Capybara.current_session.server.port}/#{ShortUrl.last.short_url})")
    expect(page).to have_link 'Return to main page', href: root_path
  end
end
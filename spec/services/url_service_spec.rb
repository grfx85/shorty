# frozen_string_literal: true

require 'rails_helper'

RSpec.describe UrlService do
  let(:service) { UrlService.new(original_url: 'www.test.com', obj: ShortUrl.new) }

  describe '#initialize' do
    it 'sets @original_url instance variable' do
      expect(service.instance_variable_get(:@original_url)).to eq 'www.test.com'
    end

    it 'sets @obj instance variable' do
      expect(service.instance_variable_get(:@obj).class).to eq ShortUrl
    end
  end

  describe '#call' do
    it 'receives #process_url' do
      expect(service).to receive(:process_url)
      service.call
    end
  end

  describe '#process_url' do
    it 'assigns original_url to obj' do
      expect { service.process_url }.to change { service.obj.original_url }
    end

    it 'service obj class receives #sanitize_original_url' do
      expect(service.obj).to receive(:sanitize_original_url)
      service.process_url
    end

    it 'returns obj class' do
      expect(service.process_url.class).to eq ShortUrl
    end

    it 'processes the original url' do
      expect(service.process_url.sanitize_url).to eq 'http://test.com'
    end
  end
end

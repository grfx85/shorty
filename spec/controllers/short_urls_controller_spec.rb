# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ShortUrlsController, type: :controller do
  describe '#GET index' do
    context 'root link entry' do
      it 'redirects to root path' do
        get :index
        expect(response).to have_http_status(:success)
        expect(flash.empty?).to eq true
      end
    end
  end

  describe '#POST' do
    context 'valid parameter' do
      before { create(:short_url, original_url: 'www.test.com') }

      it 'creates a new shot url' do
        post :create, params: { original_url: 'www.test.com' }
        expect(response).to have_http_status(:redirect)
        expect(flash.empty?).to eq false
        expect(flash[:notice]).to eq 'Short link already exists!'
        expect(response).to redirect_to(shortened_path(short_url: ShortUrl.last.short_url))
      end
    end

    context 'invalid parameter' do
      it 'dose not create new short url' do
        post :create, params: { original_url: 'aaaaa' }
        expect(response).to have_http_status(:redirect)
        expect(flash.empty?).to eq false
        expect(response).to redirect_to(root_path)
        expect(flash[:notice]).to eq '<ul><li>Original url Invalid url format!</li></ul>'
      end
    end
  end

  describe '#GET show' do
    context 'show link under the shorten link' do
      before { create(:short_url, original_url: 'www.test.com') }

      it 'redirect outside' do
        get :show, params: { short_url: ShortUrl.last.short_url }
        expect(response).to have_http_status(:redirect)
        expect(flash.empty?).to eq true
        expect(response).to redirect_to(ShortUrl.last.sanitize_url)
      end
    end
  end

  describe '#GET shortened' do
    context 'show link under the shorten link' do
      before { create(:short_url, original_url: 'www.test.com') }

      it 'renders summary shortened page' do
        get :shortened, params: { short_url: ShortUrl.last.short_url }
        expect(response).to have_http_status(:success)
        expect(flash.empty?).to eq true
      end
    end
  end
end

# frozen_string_literal: true

class ShortUrlsController < ApplicationController
  expose(:url) { ShortUrl.find_by_short_url(params[:short_url]) }
  expose(:url_service) { ::UrlService.new(original_url: params[:original_url]).call }
  expose(:host) { request.host_with_port }
  expose(:original_url) { url.sanitize_url }
  expose(:short_url) { host + '/' + url.short_url }

  def show
    redirect_to url.sanitize_url
  end

  def create
    if url_service.fresh_url?
      if url_service.save
        redirect_to shortened_path(url_service.short_url)
      else
        redirect_to root_path, notice: "<ul>#{url_service.errors.full_messages.map { |e| "<li>#{e}</li>" }.join}</ul>".html_safe
      end
    else
      redirect_to shortened_path(url_service.check_duplicates.short_url), notice: 'Short link already exists!'
    end
  end
end

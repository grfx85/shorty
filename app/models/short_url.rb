# frozen_string_literal: true

# == Schema Information
#
# Table name: short_urls
#
#  id           :bigint(8)        not null, primary key
#  original_url :text
#  short_url    :string
#  sanitize_url :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class ShortUrl < ApplicationRecord
  validates :original_url, presence: true, on: :create
  validates_format_of :original_url,
                      with: %r{\A(?:(?:http|https):\/\/)?([-a-zA-Z0-9.]{2,256}\.[a-z]{2,4})\b(?:\/[-a-zA-Z0-9@,!:%_+.~#?&\/\/=]*)?\z},
                      message: 'Invalid url format!'
  before_create :create_short_url
  before_create :sanitize_original_url

  def create_short_url
    suffix = [*('a'..'z'), *('0'..'9')].sample(10).join
    existing_short_url = ShortUrl.where(short_url: suffix).last
    if existing_short_url.present?
      create_short_url
    else
      self.short_url = suffix
    end
  end

  def check_duplicates
    ShortUrl.find_by_sanitize_url(sanitize_url)
  end

  def fresh_url?
    check_duplicates.blank?
  end

  def sanitize_original_url
    original_url.strip!
    self.sanitize_url = original_url.downcase.gsub(%r{(https?:\/\/)|(www\.)}, '')
    self.sanitize_url = "http://#{sanitize_url}"
  end
end

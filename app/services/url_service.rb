# frozen_string_literal: true

class UrlService
  attr_reader :original_url
  attr_accessor :obj

  def initialize(original_url:, obj: ShortUrl.new)
    @original_url = original_url
    @obj = obj
  end

  def call
    process_url
  end

  def process_url
    obj.original_url = original_url
    obj.sanitize_original_url
    obj
  end
end
